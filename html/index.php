<?php require_once "conn/conn.php" ?>
<!doctype html>
<html class="no-js" lang="en">
<head>
<script async>(function(s,u,m,o,j,v){j=u.createElement(m);v=u.getElementsByTagName(m)[0];j.async=1;j.src=o;j.dataset.sumoSiteId='1098bae909d86a62e295601b277ae360bef3a4e719f652fdc3d224d0f0a13574';v.parentNode.insertBefore(j,v)})(window,document,'script','//load.sumo.com/');</script>
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-THBPLN7');</script>
        <!-- End Google Tag Manager -->
    <!-- title -->
    <title>SUPERFORCE CROSSFIT</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
    <meta name="author" content="DeeperLabs">
    <!-- description -->
    <meta name="description" content="Superforce Crossfit ">
    <!-- keywords -->
    <meta name="keywords" content="creative, modern, clean, bootstrap responsive, html5, css3, portfolio, blog, agency, templates, multipurpose, one page, corporate, start-up, studio, branding, designer, freelancer, carousel, parallax, photography, personal, masonry, grid, coming soon, faq">
    <!-- favicon -->
    <link rel="shortcut icon" href="./images/home/logo.png">
    <link rel="apple-touch-icon" href="./images/home/logo.png">
    <link rel="apple-touch-icon" sizes="72x72" href="./images/home/logo.png">
    <link rel="apple-touch-icon" sizes="114x114" href="./images/home/logo.png">
    <!-- bootstrap -->
    <link rel="stylesheet" href="./css/bootstrap.min.css" />
    <!-- themify icon -->
    <link rel="stylesheet" href="./css/themify-icons.css">
    <!-- justified gallery -->
    <link rel="stylesheet" href="./css/justified-gallery.min.css">
    <!-- magnific popup -->
    <link rel="stylesheet" href="./css/magnific-popup.css" />
    <!-- bootsnav -->
    <link rel="stylesheet" href="./css/bootsnav.css">
    <link rel="stylesheet" href="./css/general.css">
    <!-- style -->
    <link rel="stylesheet" href="./css/style.css" />
    <!-- style form -->
    <!--link rel="stylesheet" href="css/responsive.css" /-->
    <!--[if IE]>
        <script src="./js/html5shiv.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

    <link href="https://fonts.googleapis.com/css?family=Barlow+Condensed:400,500,600,600i,700" rel="stylesheet">

</head>
<body>
<?php
    $alerta= $_GET['msg'];

    if ($alerta == "sucesso")
    echo "<script type='text/javascript'>
    alert('Mensagem enviada com sucesso, em  breve entraremos em contato');
</script>"; 
    ?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-THBPLN7"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="bottomBar">
        <a href="agendar">
            Agende agora sua aula experimental GRATUITA!
        </a>
    </div>
    <div class="bg">
        <!-- Static navbar -->
        <div id="inicio"></div>
        <div id="mainmenu" class="main__menu">
            <div id="closemenu" class="close__menu">

            </div>
            <ul class="menu__list">
                <li class="menu__item">
                    <a href="#inicio">
                        Início
                    </a>
                </li>

                <li class="menu__item">
                    <a href="agendar">
                        Aula Experimental
                    </a>
                </li>

                <li class="menu__item">
                    <a href="#crossfit">
                        O que é Crossfit?
                    </a>
                </li>

                <li class="menu__item">
                    <a href="#nossos-alunos">
                        Nossos Alunos
                    </a>
                </li>

                <li class="menu__item">
                    <a href="#professores">
                        Nossos Professores
                    </a>
                </li>

                <li class="menu__item">
                    <a href="#faq">
                        Perguntas Frequentes
                    </a>
                </li>

                <li class="menu__item">
                    <a href="#contato">
                        Contato
                    </a>
                </li>
            </ul>
        </div>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <div class="navbar-toggle collapsed">
                        <img id="openmenu" src="./images/home/menu.png"/>
                    </div>
                    <a class="navbar-brand" href="#">
                        <img id="logo" src="./images/home/logo.png" />
                    </a>
                </div>
            </div><!-- container-fluid -->
        </nav>   
        <!-- END HEADER -->

        <!-- NÓS SOMOS SUPER -->
        <section id="nos-somos" class="wow fadeIn no-padding text-white text-center">
            <div class="">
                <div class="intro__content">
                    <div class="custom__container">
                        <div class="title-medium font-2">NÓS SOMOS</div>
                        <div class="title-extra-large font-3"><strong>SUPER!</strong></div>
                        <div class="title-small">
                            Tem interesse em praticar CrossFit? Venha fazer uma aula experimental conosco em uma de nossas unidades e conheça os benefícios do treinamento.
                        </strong>
                        </div>
                        <a href="agendar" class="top__button btn btn-danger btn-lg active btn__red" role="button" aria-pressed="true">AGENDE UMA AULA EXPERIMENTAL!</a>
                        <div>
                            <img class="scroll-down-icon" src="./images/home/scrolldown.png" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END -->
    </div>

    <!-- CROSSFIT -->
    <section id="crossfit" class="wow fadeIn" style="background-image:url('images/home/bg-white.jpg');">
        <div class="">
            <div class="">
                <div class="">
                    <div class="custom__container">
                        <div class="title-large">O QUE É CROSSFIT?</div>
                        <div class="text-medium no-margin">O CrossFit é um programa de treinamento de condicionamento físico e força com exercícios funcionais de alta intensidade. Entre os principais benefícios, estão aumento de flexibilidade, coordenação e resistências física e cardiorrespiratória.</div>
                    </div>
                </div>
            </div>
            <div class="crossfit__icons custom__container">
                <div class="icons__item">
                    <div class="text-center"><img src="./images/home/shape-1.png"></div>
                    <div>Movimentos funcionais</div>
                </div>
                <div class="icons__item">
                    <div class="text-center"><img src="./images/home/shape-3.png"></div>
                    <div>Mais intensidade</div>
                </div>
                <div class="icons__item">
                    <div class="text-center"><img src="./images/home/shape-4.png"></div>
                    <div>Constantemente variados</div>
                </div>
            </div>

        </div>            
    </section>
    <!-- END -->

    <!-- VENHA NOS CONHECER -->
    <section id="venha-conhecer" class="wow meetus fadeIn parallax" style="background-image:url('images/home/bg-red.png');">
        <div class="custom__container text-center">
            <div class="meetus__content">
                <div class="text-white title-large meetus__title font-3"><strong>VENHA NOS CONHECER!</strong></div>
                <div class="meetus__text text-white text-medium text-center">
                    As unidades da Superforce, em Porto Alegre, são autorizadas pela CrossFit Inc. a utilizar a metodologia. Nossos box possuem toda a estrutura necessária para atender aos praticantes. São vários horários disponíveis para as aulas. Venha fazer parte da família Superforce!
                </div>
                <a href="agendar" id="singlebutton" name="singlebutton" class="btn btn-extra-large btn-black-red center-block">AGENDE UMA AULA EXPERIMENTAL</a>
            </div>
        </div>
    </section>
    <!-- END  -->

    <!-- ALGUNS DOS NOSSOS ALUNOS -->
    <section id="nossos-alunos" class="alunos wow fadeIn parallax xs-background-image-center" style="background-image: url('images/home/bg.png')">  
        <div class="custom__container" id="resultados">
            <div class="">
                <h3 class="text-white font-weight-600 h4">ALGUNS DOS NOSSOS ALUNOS</h3>
            </div> 
            <div class="result__text">
                <p class="text-medium">Crossift é para todos. Conheça um pouco mais sobre os diversos estilos de vida dos nossos alunos.</p>
            </div> 
        </div>
        <div class="">
            <div class=''>
                <div class='first__slider__container'>
                    <div class="carousel slide">
                        <!-- CAROUSEL -->
                        <div id="first-slider" class="carousel-inner">
                            <!-- 1 -->
                            <div class="first_slide__item">
                                <img src="./images/home/daiane_dias.jpg" class="img-fluid" alt="">
                                <div class="slide__text text-center text-smal">
                                    <h6>Daiane Dias</h6>
                                    <p></p>
                                </div>
                            </div>
                            <div class="first_slide__item">
                                <img src="./images/home/daiane_cardoso.jpg" class="img-fluid" alt="">
                                <div class="slide__text text-center text-smal">
                                    <h6>Daiane Cardoso</h6>
                                    <p></p>
                                </div>
                            </div>
                            <div class="first_slide__item">
                                <img src="./images/home/gilberto_souza.jpg" class="img-fluid" alt="">
                                <div class="slide__text text-center text-smal">
                                    <h6>Gilberto Souza</h6>
                                    <p></p>
                                </div>
                            </div>
                            <div class="first_slide__item">
                                <img src="./images/home/gisiane_mury.jpg" class="img-fluid" alt="">
                                <div class="slide__text text-center text-smal">
                                    <h6>Gisiane Mury</h6>
                                    <p></p>
                                </div>
                            </div>
                            <div class="first_slide__item">
                                <img src="./images/home/harlen_mota.jpg" class="img-fluid" alt="">
                                <div class="slide__text text-center text-smal">
                                    <h6>Harlen Mota</h6>
                                    <p></p>
                                </div>
                            </div>
                            <div class="first_slide__item">
                                <img src="./images/home/patricia_caye.jpg" class="img-fluid" alt="">
                                <div class="slide__text text-center text-smal">
                                    <h6>Patrícia Caye</h6>
                                    <p></p>
                                </div>
                            </div>
                            </div>
                        </div>                  
                    </div>
                </div>                          
            </div>
        </div>
    </section>
    <!-- END -->
    

    <!-- UM POUCO DO NOSSO DIA A DIA -->
    <section id="dia-a-dia" class="wow fadeIn no-padding parallax xs-background-image-center dayaday" style="background-image: url('images/home/bg.png')">
        <div class="custom__container">
            <div class="text-white font-weight-600 h3">UM POUCO DO NOSSO DIA A DIA</div>
            <p class="text-medium">
                Fotos do alunos, professores, estrutura dos box.
            </p>
        </div>

        <div class="dayayday__gallery">
                
            <div class="preview">
                <div class="preview__item">
                    <img src="./images/home/slider-3.jpg" alt="" class="preview__img">
                </div>
                <div class="preview__item">
                    <img src="./images/home/slider-2.jpg" alt="" class="preview__img">
                </div>
                <div class="preview__item">
                    <img src="./images/home/slider-1.jpg" alt="" class="preview__img">
                </div>
                <button id="triggerModal" data-toggle="modal" data-target="#exampleModal" class="more">
                    Ver Mais
                </button>
            </div>
        </div>
        <div class='numbers__part' style="display:none;">
            <div class="custom__container">
                <div class="numbers__out" >
                    <div class="numbers__item text-red title-medium text-center">
                        <div class="big__number">
                            2
                        </div>
                        <div class="numbers__content font-2">
                            2 BOX
                            <br>
                            COMPLETOS
                        </div>
                    </div>
                    <div class="numbers__item text-red title-medium text-center">
                        <div class="big__number">
                            10
                        </div>
                        <div class="numbers__content font-2">

                            10 PROFESSORES <br>
                            QUALIFICADOS
                        </div>
                    </div>       
                </div>
            </div>       
            <div class="img-bottom-fixed"><img src="./images/home/img-bottom.png" alt="" data-no-retina="">
            </div>   
        </div>    
   </section>
   <!-- END -->
<div style="display:none" 
   <!-- NOSSOS PROFISSIONAIS -->
    <section id="professores" class="desk-half">
        <section class="wow ourpro fadeIn no-padding parallax xs-background-image-center" style="background-image: url('images/home/bg-white.jpg')">  
            <div class="pro__section" id="resultados">
                <div class="text-left text-center">
                    <h5 class="pro__main__title font-weight-300 no-margin text-black font-2">NOSSOS </h5>
                    <h3 class="pro__second__title font-weight-600 no-margin text-black font-3"> PROFISSIONAIS</h3>
                    <p>
                </div> 
            </div>    
            
            <div class="">
                <div>
                    <div id="pro__carousel" class="pro__carousel slide">
                        <div class="pro__carousel__item">
                            <img src="./images/home/img-treinador.png" alt="First slide">
                            <div class="pro__carousel__text">
                                <h4 class="pro__text__top font-2">
                                    COACH
                                </h4>
                                <h3 class="pro__text__name font-3">
                                    Cleber Souza
                                </h3>
                                <div class="pro__text__content">
                                    Formado em educação física, é especialista em atividades aeróbicas e integra a equipe competitiva da Superforce.
                                </div>
                            </div>
                        </div>
                        <div class="pro__carousel__item">
                            <img src="./images/home/img-treinador.png" alt="First slide">
                            <div class="pro__carousel__text">
                                <h4 class="pro__text__top font-2">
                                    COACH
                                </h4>
                                <h3 class="pro__text__name font-3">
                                    Cleber Souza
                                </h3>
                                <div class="pro__text__content">
                                    Formado em educação física, é especialista em atividades aeróbicas e integra a equipe competitiva da Superforce.
                                </div>
                            </div>
                        </div>
                        <div class="pro__carousel__item">
                            <img src="./images/home/img-treinador.png" alt="First slide">
                            <div class="pro__carousel__text">
                                <h4 class="pro__text__top font-2">
                                    COACH
                                </h4>
                                <h3 class="pro__text__name font-3">
                                    Cleber Souza
                                </h3>
                                <div class="pro__text__content">
                                    Formado em educação física, é especialista em atividades aeróbicas e integra a equipe competitiva da Superforce.
                                </div>
                            </div>
                        </div>
                        <div class="pro__carousel__item">
                            <img src="./images/home/img-treinador.png" alt="First slide">
                            <div class="pro__carousel__text">
                                <h4 class="pro__text__top font-2">
                                    COACH
                                </h4>
                                <h3 class="pro__text__name font-3">
                                    Cleber Souza
                                </h3>
                                <div class="pro__text__content">
                                    Formado em educação física, é especialista em atividades aeróbicas e integra a equipe competitiva da Superforce.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END -->

        <!-- BLOG -->
        <section id="blog" class="blog__section wow fadeIn parallax xs-background-image-center">
            <div class="custom__container">
                <h3 class="text-white font-weight-600 h3">BLOG
                </h3>
                <p class="text-medium text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </p>
            </div>
            <div id="blog__slider" class='blog__slider'>
            <?php do { ?>
                <div class="blog__slider__item">
                    <img src="./images/home/blog-slider-girl-hor.png" alt="">
                    <div class="blog__item__content">
                        <h4 class="h4 blog__item__title">
                        <?php echo utf8_encode($blog['post_title']); ?>
                        </h4>
                        <a href="/blog_post" class="blog__button">
                            Leia a postagem!
                        </a>
                    </div>
                </div>
                <?php } while($blog = $result->fetch_assoc()); ?>
            </div>
            <div class="custom__container">
                <a class="see__more h5" href="/blog">veja todas as postagens 🡢</a>
            </div>
        </section>
        <!-- END -->
    </section>
            </div>

    <!-- NOSSOS PARCEIROS -->
    <section id="nossos-parceiros" class="partners__section wow fadeIn parallax xs-background-image-center">
        <div class="custom__container">
            <div class="" >
                <h2 class="text-white font-weight-600 h4">PARCEIROS</h2>
                <div class='partners__icons'>
                    <div class="partners__icon">
                        <div class="partners__hover">
                            <div class="hover_out link">
                                <a href="#" target="_blank">
                                    <img src="./images/home/link-icon.png" alt="">
                                </a>
                            </div>
                            <div class="divisor"></div>
                            <div class="hover_out instagram">
                                <a href="#" target="_blank">
                                    <img src="./images/home/instagram-logo.png" alt="">
                                </a>
                            </div>
                        </div>
                        <img src="./images/home/spice-food.png" style="" /> 
                    </div>
                    <div class="partners__icon">
                        <div class="partners__hover">
                            <div class="link">
                                <a href="#" target="_blank">
                                    <img src="./images/home/link-icon.png" alt="">
                                </a>
                            </div>
                            <div class="divisor"></div>
                            <div class="instagram">
                                <a href="#" target="_blank">
                                    <img src="./images/home/instagram-logo.png" alt="">
                                </a>
                            </div>
                        </div>
                        <img src="./images/home/cilios-de-guria.png" style=""/> 
                    </div>
                    <div class="partners__icon">
                        <div class="partners__hover">
                            <div class="link">
                                <a href="#" target="_blank">
                                    <img src="./images/home/link-icon.png" alt="">
                                </a>
                            </div>
                            <div class="divisor"></div>
                            <div class="instagram">
                                <a href="#" target="_blank">
                                    <img src="./images/home/instagram-logo.png" alt="">
                                </a>
                            </div>
                        </div>
                        <img src="./images/home/nutriforce.png" style="" />
                    </div>
                    <div class="partners__icon">
                        <div class="partners__hover">
                            <div class="link">
                                <a href="#" target="_blank">
                                    <img src="./images/home/link-icon.png" alt="">
                                </a>
                            </div>
                            <div class="divisor"></div>
                            <div class="instagram">
                                <a href="#" target="_blank">
                                    <img src="./images/home/instagram-logo.png" alt="">
                                </a>
                            </div>
                        </div>
                        <img src="./images/home/xavier.png" style="" />
                    </div>
                </div>    
            </div>
        </div>  
    </section>
   <!-- END -->


    <!-- QUESTÕES RECORRENTES -->
    <section id="faq" class="faq__section wow fadeIn parallax xs-background-image-center">
        <div class="custom__container">
            <h4 class="faq__title text-white font-weight-600 h4">ALGUMAS QUESTÕES RECORRENTES
            </h4>
            <div class="">
                <div class="text-left page-header">
                    <span class="faq__question">Qualquer um pode treinar?</span>
                    <p class="faq__ans">Sim! Uma das principais vantagens do CrossFit é que ele pode ser adaptado à realidade de qualquer pessoa. Crianças, adultos e idosos, independente do condicionamento físico, podem começar a treinar a qualquer momento.</p>
                </div>

                <div class="text-left page-header">
                    <span class="faq__question">Preciso estar em forma para treinar?</span>
                    <p class="faq__ans">Não! Mesmo que você não esteja em forma e ainda não possua resistência física, é possível praticar o CrossFit. Isso porque, ao ingressar nas aulas, o aluno inicia um treinamento mais leve, com exercícios adaptados, de acordo com sua capacidade física. Na SuperForce prezamos a muito pela parte técnica, visamos uma evolução gradual sem pular etapas.</p>
                </div>

                <div class="text-left page-header">
                    <span class="faq__question">Em quantos meses de treino eu consigo baixar meu percentual de gordura?</span>
                    <p class="faq__ans">Isso varia para cada indivíduo. A redução do percentual de gordura depende da atividade física e da alimentação. Com uma dieta equilibrada e frequência nos treinamentos, no primeiro mês, já é possível observar resultados. O praticante consegue, dessa maneira, diminuir a gordura e aumentar a massa muscular.</p>
                </div>

                <div class="text-left page-header">
                    <span class="faq__question">Qual os principais benefícios da prática do CrossFit?</span>
                    <p class="faq__ans">O CrossFit traz vários benefícios aos praticantes. De forma geral, ele proporciona saúde e bem-estar, reduz os níveis de estresse e auxilia na perda de peso. Por ser um exercício que estimula vários grupos musculares ao mesmo tempo, o Crossfit também aumenta as resistências física e cardiovascular. </p>
                </div>

                <div class="text-left page-header">
                    <span class="faq__question">Quanto tempo dura uma aula de CrossFit?</span>
                    <p class="faq__ans">Uma aula de CrossFit tem duração média de uma hora, dividida em aquecimento, parte técnica, série de força e WOD (Treino do Dia)</p>
                </div>
            </div>
        </div>  
    </section>
   <!-- END -->


   <!-- DÚVIDAS -->
    <section id="contato" class="contact__section">
        <div class="custom__container">
            <h2 class="text-white font-weight-600 h4">
                FICOU COM ALGUMA DÚVIDA?
            </h2>
            <div class="">
                <p class="contact__text text-white">
                    Se você tiver qualquer dúvida sobre o CrossFit e os treinamentos da Superforce, sinta-se à vontade para entrar em contato conosco por e-mail. Caso prefira, você também pode visitar uma de nossas unidades para conversar diretamente com nossos instrutores.
                </p>
                <div class="text-left">
                    <form role="form" class="title-left" action="contato.php" method="post">
                        <div class="">
                            <label class="contact__label" for="name">NOME</label>
                            <input id="name" class="contact__input" type="text" name="nome">
                        </div>

                        <div class="">
                            <label class="contact__label" for="email">E-MAIL</label>
                            <input id="email" class="contact__input" type="text" name="email">
                        </div>

                        <div class="">
                            <label class="contact__label" for="message" >MENSAGEM</label>
                            <input id="message" class="contact__input" type="text" name="mensagem">
                        </div>
                        <div class="text-right">
                            <input class="submit__btn btn__red" type="submit" value="Enviar">
                        </div>
                    </form>
                </div>
            </div> 
        </div>
    </section>
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="gallery_modal" id="gallery">

                    <div class="gallery__item">
                        <img src="./images/home/slider-3.jpg" alt="" class="preview__img">
                    </div>
                    <div class="gallery__item">
                        <img src="./images/home/slider-2.jpg" alt="" class="preview__img">
                    </div>
                    <div class="gallery__item">
                        <img src="./images/home/slider-1.jpg" alt="" class="preview__img">
                    </div>
                    <div class="gallery__item">
                        <img src="./images/home/slider-4.jpeg" alt="" class="preview__img">
                    </div>
                    <div class="gallery__item">
                        <img src="./images/home/slider-5.jpeg" alt="" class="preview__img">
                    </div>
                    <div class="gallery__item">
                        <img src="./images/home/slider-6.jpeg" alt="" class="preview__img">
                    </div>
                    <div class="gallery__item">
                        <img src="./images/home/slider-7.jpeg" alt="" class="preview__img">
                    </div>
                    <div class="gallery__item">
                        <img src="./images/home/slider-8.jpeg" alt="" class="preview__img">
                    </div>
                </div>
            </div>
            </div>
        </div>

    <!-- END -->
    <!-- start scroll to top -->
    <a class="scroll-top-arrow" href="javascript:void(0);"><i class="ti-arrow-up"></i></a>
    <!-- end scroll to top -->
    <!-- javascript libraries -->
    <script type="text/javascript" src="./js/jquery.js"></script>
    <script type="text/javascript" src="./js/modernizr.js"></script>
    <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./js/smooth-scroll.js"></script>
    <script type="text/javascript" src="./js/jquery.appear.js"></script>
    <!-- menu navigation -->
    <script type="text/javascript" src="./js/bootsnav.js"></script>
    <!-- page scroll -->
    <script type="text/javascript" src="./js/page-scroll.js"></script>
    <script type="text/javascript" src="./js/main.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

    <script>
        let justOpened = false;
        let halfHeight = $(window).height()/2;
        $(document).scroll(function() {
            var y = $(this).scrollTop();
            if (!justOpened) {
                if (y >= halfHeight) {
                    $('.bottomBar').show();
                    setTimeout(()=> {
                        $('.bottomBar').addClass('on');
                    }, 20)
                    let justOpened = true;
                }
                else {
                    $('.bottomBar').removeClass('on');
                    setTimeout(()=> {
                        $('.bottomBar').hide();
                    }, 400)
                    let justOpened = true;

                }
                setTimeout(()=> {
                    let justOpened = false;
                }, 1000)
            }
        });

        $('.menu__item a').on('click', ()=> {
            $('#mainmenu').removeClass('opened');
            setTimeout(() => {
                $('#mainmenu').hide();
            }, 250);
        })

        $('#closemenu').on('click', () => {
            $('#mainmenu').removeClass('opened');
            setTimeout(() => {
                $('#mainmenu').hide();
            }, 250);
        });

        $('#openmenu').on('click', () => {
            $('#mainmenu').css('display', 'flex')
            setTimeout(() => {
                $('#mainmenu').addClass('opened');
            }, 10);
        });

        $('#first-slider').slick({
            // infinite: false,
            centerMode: true,
            slidesToShow: 3,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerPadding: '40px',
                        slidesToShow: 1,
                    }
                }
            ]
        });

        $('#pro__carousel').slick({
            arrows: false,
            dots: true,
            infinite: false,
        })
        setTimeout(() => {
            $('#blog__slider').slick({
                infinite: false
            })
        }, 1000)
        $('#triggerModal').on('click', function() {
            setTimeout(function() {
                $('#gallery').slick({
                    infinite: false
                })
            },600)
        });
        </script>
</body>
</html>