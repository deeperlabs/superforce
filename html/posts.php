<?php require_once "blog/conn.php" ?>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <!-- title -->
        <title>Blog Impulso - Os melhores conteúdos para você crescer</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
        <meta name="author" content="Deeper Labs">
        <!-- description -->
        <meta name="description" content="">
        <!-- keywords -->
        <meta name="keywords" content="creative, modern, clean, bootstrap responsive, html5, css3, portfolio, blog, agency, templates, multipurpose, one page, corporate, start-up, studio, branding, designer, freelancer, carousel, parallax, photography, personal, masonry, grid, coming soon, faq">
        <!-- favicon -->
        <link rel="shortcut icon" href="images/favicon.png">
        <link rel="apple-touch-icon" href="images/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
        <!-- animation -->
        <link rel="stylesheet" href="css/animate.css" />
        <!-- bootstrap -->
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <!-- et line icon --> 
        <link rel="stylesheet" href="css/et-line-icons.css" />
        <!-- font-awesome icon -->
        <link rel="stylesheet" href="css/font-awesome.min.css" />
        <!-- themify icon -->
        <link rel="stylesheet" href="css/themify-icons.css">
        <!-- swiper carousel -->
        <link rel="stylesheet" href="css/swiper.min.css">
        <!-- justified gallery  -->
        <link rel="stylesheet" href="css/justified-gallery.min.css">
        <!-- magnific popup -->
        <link rel="stylesheet" href="css/magnific-popup.css" />
        <!-- revolution slider -->
        <link rel="stylesheet" type="text/css" href="revolution/css/settings.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="revolution/css/layers.css">
        <link rel="stylesheet" type="text/css" href="revolution/css/navigation.css">
        <!-- bootsnav -->
        <link rel="stylesheet" href="css/bootsnav.css">
        <!-- style -->
        <link rel="stylesheet" href="css/style.css" />
        <!-- style form -->
        <link rel="stylesheet" href="css/form.css" />
        <!-- responsive css -->
        <link rel="stylesheet" href="css/responsive.css" />
        <!--[if IE]>
            <script src="js/html5shiv.js"></script>
        <![endif]-->
    </head>
    <body>
<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/4224326.js"></script>
<!-- End of HubSpot Embed Code -->
        <!-- start header -->
        <header>
        <!-- start navigation -->
        <nav class="navbar navbar-default bootsnav navbar-fixed-top header-light bg-transparent white-link">
            <div class="container nav-header-container">
                <div class="row">
                    <!-- start logo -->
                    <div class="col-md-2 col-xs-5">
                        <a href="../index.php" title="Pofo" class="logo"><img src="images/logo.png" data-at2x="images/logo@2x.png" class="logo-dark" alt="Pofo"><img src="images/logo-white.png" data-at2x="images/logo-white@2x.png" alt="Pofo" class="logo-light default"></a>
                    </div>
                    <!-- end logo -->
                    <div class="col-md-7 col-xs-2 width-auto pull-right accordion-menu xs-no-padding-right">
                        <button type="button" class="navbar-toggle collapsed pull-right" data-toggle="collapse" data-target="#navbar-collapse-toggle-1">
                            <span class="sr-only">toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="navbar-collapse collapse pull-right" id="navbar-collapse-toggle-1">
                            <ul id="accordion" class="nav navbar-nav navbar-left no-margin alt-font text-normal" data-in="fadeIn" data-out="fadeOut">
                                <!-- start menu item -->
                                <li class="dropdown megamenu-fw">
                                    <a href="../index.php">Home</a><i class="fa fa-angle-down dropdown-toggle" data-toggle="dropdown" aria-hidden="true"></i>
                                      </li>
                                      <li class="dropdown megamenu-fw">
                                    <a href="index.php">Blog</a><i class="fa fa-angle-down dropdown-toggle" data-toggle="dropdown" aria-hidden="true"></i>
                                      </li> 
                                <!-- end menu item -->            
                        </div>
                    </div>
                    <div style="display:none" class="col-md-2 col-xs-5 width-auto">
                        <div class="header-searchbar">
                            <a href="#search-header" class="header-search-form"><i class="fa fa-search search-button"></i></a>
                            <!-- search input-->
                            <form id="search-header" method="post" action="search-result.html" name="search-header" class="mfp-hide search-form-result">
                                <div class="search-form position-relative">
                                    <button type="submit" class="fa fa-search close-search search-button"></button>
                                    <input type="text" name="search" class="search-input" placeholder="Enter your keywords..." autocomplete="off">
                                </div>
                            </form>
                        </div>
                        <div class="heder-menu-button sm-display-none">
                            <button class="navbar-toggle mobile-toggle right-menu-button" type="button" id="showRightPush">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <!-- end navigation --> 
        <div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2">
            <button class="close-button-menu side-menu-close" id="close-pushmenu"></button>
            <div class="display-table padding-twelve-all height-100 width-100 text-center">
                <div class="display-table-cell vertical-align-top padding-70px-top position-relative">
                    <div class="row">
                        <div class="col-lg-12 margin-70px-bottom">
                            <img src="images/logo-black-big.png" alt="" />
                        </div>
                        <div class="col-lg-12 margin-70px-bottom">
                            <img src="images/sidebar-image1.png" alt="" />
                        </div>
                        <div class="col-lg-12">
                            <h5 class="alt-font text-extra-dark-gray"><span class="display-block font-weight-300 text-dark-gray">The world's most</span><strong>powerful website builder.</strong></h5>
                        </div>
                        <div class="col-lg-12">
                            <a href="javascript:void(0);" class="btn btn-deep-pink btn-small text-extra-small border-radius-4"><i class="fa fa-play-circle icon-very-small margin-5px-right no-margin-left" aria-hidden="true"></i>Purchase Now</a>
                        </div>
                        <div class="col-md-12 margin-100px-top text-center">
                            <div class="icon-social-medium margin-three-bottom">
                                <a href="https://www.facebook.com/" target="_blank" class="text-extra-dark-gray text-deep-pink-hover margin-one-lr"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <a href="https://twitter.com/" target="_blank" class="text-extra-dark-gray text-deep-pink-hover margin-one-lr"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                <a href="https://dribbble.com/" target="_blank" class="text-extra-dark-gray text-deep-pink-hover margin-one-lr"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
                                <a href="https://plus.google.com" target="_blank" class="text-extra-dark-gray text-deep-pink-hover margin-one-lr"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                <a href="https://www.tumblr.com/" target="_blank" class="text-extra-dark-gray text-deep-pink-hover margin-one-lr"><i class="fa fa-tumblr" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end navigation -->  
    </header>
    <!-- end header -->
        <!-- start page title section -->
        <section class="wow fadeIn parallax" data-stellar-background-ratio="0.5" style="background-image:url('images/blog.png');">
            <div class="opacity-medium bg-extra-dark-gray"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 extra-small-screen display-table page-title-large">
                        <div class="display-table-cell vertical-align-middle text-center">
                            <!-- start page title -->
                            <h1 class="text-white alt-font font-weight-600 letter-spacing-minus-1 margin-10px-bottom">Blog Impulso</h1>
                            <span class="text-white opacity6 alt-font">Conteúdo de qualidade para você crescer</span>
                            <!-- end page title --> 
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end page title section --> 
        <!-- start post content section -->  
        <section class="wow fadeIn">
            <div class="container"> 
                <div class="row col-4-nth sm-col-2-nth">
                <?php do { ?>
                    <?php
                    $id_post = $blog4['ID'];
                    //QUERY BLOG IMAGES
                     @$sql_image = "select * from wphi_posts WHERE post_type='attachment' AND post_parent= '$id_post' AND post_title LIKE 'cover_%' ORDER BY ID DESC ";
                     $result_image = $conn->query($sql_image );
                     $blog_image = $result_image->fetch_assoc();
                                
                     ?>
                    <!-- start post item -->
                    <div class="col-md-3 col-sm-6 col-xs-12 margin-50px-bottom last-paragraph-no-margin xs-margin-30px-bottom wow fadeInUp">
                        <div class="blog-post blog-post-style1 xs-text-center">
                            <div class="blog-post-images overflow-hidden margin-25px-bottom sm-margin-20px-bottom">
                                <a href="post.php?t=<?php echo wordwrap(utf8_encode($blog4['post_title']), 1, '-', 0); ?>&id=<?php echo $id_post; ?>">
                                    <img src="<?php echo utf8_encode($blog_image['guid']); ?>" alt="">
                                </a>
                            </div>
                            <div class="post-details">
                                <span class="post-author text-extra-small text-medium-gray text-uppercase display-block margin-10px-bottom xs-margin-5px-bottom"><?php echo mb_strimwidth($blog4['post_date'], 0, 10); ?> | POR <a href="#" class="text-medium-gray">Impulso</a></span>
                                <a href="post.php?t=<?php echo wordwrap(utf8_encode($blog4['post_title']), 1, '-', 0); ?>&id=<?php echo $id_post; ?>" class="post-title text-medium text-extra-dark-gray width-90 display-block sm-width-100"><?php echo utf8_encode($blog4['post_title']); ?></a>
                                <div class="separator-line-horrizontal-full bg-medium-light-gray margin-20px-tb sm-margin-15px-tb"></div>
                            </div>
                        </div>
                    </div>
                    <!-- end post item -->
                    <?php } while($blog4 = $result4->fetch_assoc()); ?>
                </div>
                <!-- start slider pagination -->
                <div class=" text-center margin-100px-top sm-margin-50px-top wow fadeInUp">
                    <div class="pagination text-small text-uppercase text-extra-dark-gray">
                        <ul>
                            <li><a href="#">Ver mais</a></li>
                        </ul>
                    </div>
                </div>
                <!-- end slider pagination -->
            </div>
        </section>  
        <!-- end blog content section -->  
        
      <!-- start footer --> 
      <footer class="footer-standard-dark bg-extra-dark-gray" id="contato"> 
      <div class="footer-widget-area padding-five-tb xs-padding-30px-tb">
          <div class="container">
              <div class="row equalize xs-equalize-auto">
                  <div class="col-md-3 col-sm-6 col-xs-12 widget border-right border-color-medium-dark-gray sm-no-border-right sm-margin-30px-bottom xs-text-center">
                      <!-- start logo -->
                      <a href="#" class="margin-20px-bottom display-inline-block"><img class="footer-logo" src="images/logo-white.png" data-at2x="images/logo-white@2x.png" alt="Pofo"></a>
                      <!-- end logo -->
                      <p class="text-small width-95 xs-width-100">Agência de Marketing digital, focada em Growth Hacking</p>
                      <!-- start social media -->
                      <div class="social-icon-style-8 display-inline-block vertical-align-middle">
                          <ul class="small-icon no-margin-bottom">
                              <li><a class="facebook text-white" href="https://www.facebook.com/daumimpulso" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                              <li><a class="google text-white" href="https://www.linkedin.com/company/daumimpulso/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                              <li><a class="instagram text-white" href="https://www.instagram.com/daumimpulso/" target="_blank"><i class="fa fa-instagram no-margin-right" aria-hidden="true"></i></a></li>
                          </ul>
                      </div>
                      <!-- end social media -->
                  </div>
  
                  <div class="col-md-9 col-sm-6 col-xs-12 widget xs-text-center">
                      <form class="footer__form" action="form/send.php" method="post">
                          <h2 class="form__title">
                              Fale Conosco
                          </h2>
                          <div class="col-md-6 input__box active">
                              <input required id="name" type="text" class="footer__input"  name="nome" placeholder=" ">
                              <label class="footer__label" for="name">* Seu <strong>Nome</strong> </label>
                          </div>
                          <div class="col-md-6 input__box">
                              <input required id="email" type="email" class="footer__input"  name="email" placeholder=" ">
                              <label class="footer__label" for="email">* Seu <strong>Email</strong> </label>
                          </div>
  
                          <div class="col-md-6 input__box">
                              <input required id="phone" type="tel" class="footer__input" name="tel" placeholder=" ">
                              <label class="footer__label" for="phone"> Seu <strong>Telefone</strong> </label>
                          </div>
                          <div class="col-md-6 input__box">
                              <input required id="subject" type="text" class="footer__input" name="assunto" placeholder=" ">
                              <label class="footer__label" for="subject">*<strong>Assunto</strong> </label>
                          </div>
                          <div class="col-md-12 input__box">
                              <textarea required id="message" type="text" class="footer__input textarea"  name="mensagem" placeholder=" "></textarea>
                              <label class="footer__label" for="message">* Sua <strong>Mensagem</strong> </label>
                          </div>
  
                          <input type="submit" class="form__button" value="Enviar">
                      </form>
                  </div>
              </div>
          </div>
      </div>
      <div class="bg-dark-footer padding-50px-tb text-center xs-padding-30px-tb">
          <div class="container">
              <div class="row">
                  <!-- start copyright -->
                  <div class="col-md-6 col-sm-6 col-xs-12 text-left text-small xs-text-center">© 2018 Impulso <a href="http://daumimpulso.com.br" target="_blank" class="text-dark-gray">Innovation Agency</a></div>
                  <div class="col-md-6 col-sm-6 col-xs-12 text-right text-small xs-text-center">
                      <a href="javascript:void(0);" class="text-dark-gray">Term and Condition</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="javascript:void(0);" class="text-dark-gray">Privacy Policy</a>
                  </div>
                  <!-- end copyright -->
              </div>
          </div>
      </div>
  </footer>
           
          <!-- end footer -->
          <!-- smartbar -->
    <div id="smartbar" style="z-index:30; width:100%; height:60px; position:fixed; bottom:0px; background-color:#D40003; display:flex; align-items:center; justify-content:center; text-align:center;"><a href="https://api.whatsapp.com/send?phone=5551993771524&text=Quero%20dar%20uma%20impulsionada%20no%20meu%20neg%C3%B3cio" target="_new"><span style="font-family:Roboto; font-size:18px; color:#FFFFFF;">Você tem uma oportunidade e quer falar com a gente? </span></a> <div style="margin-left:10px;"><img src="images/whatsapplogo.png" width="40" height="40" alt=""/></div></div>
    <!-- end smartbar -->
        <!-- start scroll to top -->
        <a class="scroll-top-arrow" href="javascript:void(0);"><i class="ti-arrow-up"></i></a>
        <!-- end scroll to top  -->
        <!-- javascript libraries -->
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/modernizr.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="js/skrollr.min.js"></script>
        <script type="text/javascript" src="js/smooth-scroll.js"></script>
        <script type="text/javascript" src="js/jquery.appear.js"></script>
        <!-- menu navigation -->
        <script type="text/javascript" src="js/bootsnav.js"></script>
        <script type="text/javascript" src="js/jquery.nav.js"></script>
        <!-- animation -->
        <script type="text/javascript" src="js/wow.min.js"></script>
        <!-- page scroll -->
        <script type="text/javascript" src="js/page-scroll.js"></script>
        <!-- swiper carousel -->
        <script type="text/javascript" src="js/swiper.min.js"></script>
        <!-- counter -->
        <script type="text/javascript" src="js/jquery.count-to.js"></script>
        <!-- parallax -->
        <script type="text/javascript" src="js/jquery.stellar.js"></script>
        <!-- magnific popup -->
        <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
        <!-- portfolio with shorting tab -->
        <script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
        <!-- images loaded -->
        <script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
        <!-- pull menu -->
        <script type="text/javascript" src="js/classie.js"></script>
        <script type="text/javascript" src="js/hamburger-menu.js"></script>
        <!-- counter  -->
        <script type="text/javascript" src="js/counter.js"></script>
        <!-- fit video  -->
        <script type="text/javascript" src="js/jquery.fitvids.js"></script>
        <!-- equalize -->
        <script type="text/javascript" src="js/equalize.min.js"></script>
        <!-- skill bars  -->
        <script type="text/javascript" src="js/skill.bars.jquery.js"></script> 
        <!-- justified gallery  -->
        <script type="text/javascript" src="js/justified-gallery.min.js"></script>
        <!--pie chart-->
        <script type="text/javascript" src="js/jquery.easypiechart.min.js"></script>
        <!-- instagram -->
        <script type="text/javascript" src="js/instafeed.min.js"></script>
        <!-- retina -->
        <script type="text/javascript" src="js/retina.min.js"></script>
        <!-- revolution -->
        <script type="text/javascript" src="revolution/js/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="revolution/js/jquery.themepunch.revolution.min.js"></script>
        <!-- revolution slider extensions (load below extensions JS files only on local file systems to make the slider work! The following part can be removed on server for on demand loading) -->
        <!--<script type="text/javascript" src="revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="revolution/js/extensions/revolution.extension.video.min.js"></script>-->
        <!-- setting -->
        <script type="text/javascript" src="js/main.js"></script>
    </body>
</html>