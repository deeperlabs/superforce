<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'superfor_wp82');

/** MySQL database username */
define('DB_USER', 'superfor_wp82');

/** MySQL database password */
define('DB_PASSWORD', '09p@7lS6I-');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'fg4wg4fslpnm8y0weetv2s6gqihftx5d3fmtm7qj4ufucacsyqgqik1c0lkkxc8r');
define('SECURE_AUTH_KEY',  'hdl6gprdkbuvic0skekwoqhdrektklmd2qzrrnhsy9q9mq0thkqppuuk1nqq5cta');
define('LOGGED_IN_KEY',    'rkopegjxzvlnqirsva6bcjhvxpnhaakcfr5kotut9p5ilqegmvxcj0gwlpbqik3b');
define('NONCE_KEY',        'wvps97xsxpajamdquvo0vi4zwoiqp8gswhkzj4pjl7kymdaor8hrlo2gfavnfejj');
define('AUTH_SALT',        'nylpulizdsgi1jt5nrfvl2hbp6fxj392kkqaqimsttjexgafuwpnjpb4vpntovix');
define('SECURE_AUTH_SALT', 'xevfnqs5pu2adzofrru8gy1iifmrimyvuzmoncoirymji2thri4wydp8jsjuhtfn');
define('LOGGED_IN_SALT',   'mbkflal72pnoakfsrmbgnxnjebzfmve4bbo0bwneqqdbcm8kuofeesxbdquamyml');
define('NONCE_SALT',       'xdbbz6xxaebubeytnfr5tpwuz5ugxkq3syth3wbqmycroygkngtavuyuykd3l61n');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpns_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
