<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';

$name = ($_POST['name']);
$email = ($_POST['email']);
$phone = ($_POST['phone']);
$franchise = ($_POST['franchise']);
$date = ($_POST['date']);
$time = ($_POST['time']);
    
$txt = $name."<br><br>".$email."<br><br>".$phone."<br><br>".$franchise."<br><br>".$date."<br><br>".$time;

$to = "comercial@superforcecrossfit.com";
if ($_POST['franchise'] == 'Unidade Três Figueiras') {
    $to = 'tresfigueiras@superforcecrossfit.com';
}
    


$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 1;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'contato@superforcecrossfit.com';                 // SMTP username
    $mail->Password = 'sfrc380380';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom($to, 'Contato');
    $mail->addAddress($to, 'Contato');     // Add a recipient

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = "Aula experimental - ".$name;
    $mail->Body    = $txt;
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}
